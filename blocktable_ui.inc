<?php

/**
 * @file
 * Blocktable UI functions.
 *
 * Before using Blocktable, the user (an administrator or designer) must
 * assign blocks to a region, and order the blocks as they are to be placed
 * in the table.
 *
 * Then, to use Blocktable, the user must:
 * - Choose a region
 * - Provide table markup
 * - Optionally restrict the Blocktable to certain paths
 *
 * The UI presents forms to enable the user to do those tasks.
 *
 */

/**
 * Main Blocktable configuration form builder.
 *
 * Gets theme, region, block and blocktable data from the DB.
 *
 * Gets region choice, table markup, and (optionally) path restrictions
 * from the user, typically an admistrator or designer.
 *
 * If successful, sets blocktable_definitions in the variable table.
 * This variable may contain definitions for more than one region.
 *
 * @param $form_state
 *  The multiform state argument.
 */
function blocktable_multiform(&$form_state = NULL) {
  // If $form_state is not set, we are on step 1. Initialize.
  if (!isset($form_state['values'])) {
    $form_state['storage']['step'] = 1;
    // Get the theme. Global $theme doesn't seem to be set in this situation.
    $form_state['storage']['theme'] = variable_get('theme_default', 'garland');
    $theme = $form_state['storage']['theme'];

    // Get an array of regions.
    $form_state['storage']['regions'] = system_region_list($theme);

    // Get block info for each region.
    foreach ($form_state['storage']['regions'] as $region_key => $region_value) {
      $form_state['storage'][$region_key]['blocks'] = blocktable_get_blocks($region_key, $theme);
      $form_state['storage']['definitions'] = variable_get("blocktable_definitions", NULL);
    }
  }
  $regions = $form_state['storage']['regions'];
  $theme = $form_state['storage']['theme'];
  $step = $form_state['storage']['step'];

  // Customize the fieldset title to indicate the current step to the user.
  $form['step_message'] = array(
    '#type' => 'fieldset',
    '#title' => t('Blocktable Configuration Step @number', array('@number' => $step))
  );

  switch ($step) {
    case 1:
      // Select region.
      // This step also doubles as a summary info page about current blocktables.

      add_step_1_fields($form, $form_state);

      break;
    case 2:
      // Get table markup and path restrictions.
      add_step_2_fields($form, $form_state);

      break;
  }

  $form['step_message']['submit'] = array(
    '#type' => 'submit',
    '#value' => ($step == 2) ? t('Save') : t('Next'),
  );

  return $form;

}

/**
 * Validate handler for form ID 'blocktable_form'.
 *
 * Ensure that all the blocks are present and in the right order.
 *
 * @param $form
 *  The multiform form argument.
 * @param $form_state
 *  The multiform state argument.
 */
function blocktable_multiform_validate($form, &$form_state) {
  $step = $form_state['storage']['step'];
  switch ($step) {
    case 1:
      break;
    case 2:
      $definition = array();
      $definition['theme']      = $form_state['storage']['theme'];
      $definition['region']     = $form_state['storage']['region'];
      $definition['markup']     = $form_state['values']['markup'];
      $definition['visibility'] = $form_state['values']['visibility'];
      $definition['pages']      = $form_state['values']['pages'];
      if (!blocktable_valid($definition, $error_string)) {
        // $error_string has already been passed through t().
        form_set_error('', $error_string, TRUE);
      }
  }
}

/**
 * Submit handler for form ID 'blocktable_multiform'.
 * @param $form
 *  The multiform form argument.
 * @param $form_state
 *  The multiform state argument.
 */
function blocktable_multiform_submit($form, &$form_state) {
  $step = $form_state['storage']['step'];
  switch ($step) {
    case 1:
      $form_state['storage']['region']  = $form_state['values']['region'];
      $form_state['storage']['step']++;
      return;
      break;
    case 2;
      $region = $form_state['storage']['region'];
      $definition = array(
        'theme'      => $form_state['storage']['theme'],
        'region'     => $region,
        'visibility' => $form_state['values']['visibility'],
        'pages'      => $form_state['values']['pages'],
        'markup'     => $form_state['values']['markup'],
      );

      // Save/update blocktable definition in 'variable' table.
      $definitions = $form_state['storage']['definitions'];
      $definitions[$region] = $definition;
      variable_set("blocktable_definitions", $definitions);

      // All done.
      drupal_set_message(t('Blocktable for region @region has been saved.',
        array('@region' => $region)));
      unset($form_state['storage']);
      $form_state['redirect'] = 'admin/build/blocktable';
  }
}

/**
 * Builds form fields.
 *
 * @param $form
 *  The multiform form argument.
 * @param $form_state
 *  The multiform state argument.
 */
function add_step_1_fields(&$form, $form_state) {
  // Group the regions according to blocks and blocktables.
  $options   = array();
  $regions = $form_state['storage']['regions'];
  $theme   = $form_state['storage']['theme'];
  $definitions = $form_state['storage']['definitions'];
  foreach ($regions as $region_key => $region_value) {
    if ($definitions[$region_key]) {
      $definition = $definitions[$region_key];
      $invalid = (blocktable_valid($definition, &$error_string)) ? '' : t(' - Currently invalid');
      $options[$region_key] = $region_key . t(' - Edit existing blocktable') . $invalid;
    }
    else { // No existing blocktable, see if region has blocks.
      if (count($form_state['storage'][$region_key]['blocks'])) {
        $options[$region_key] = $region_key . t(' - Create new blocktable');
      }
      else {
        // No blocktable and no blocks, ignore this region.
      }
    }
  }

  $form['step_message']['region'] = array(
    '#type' => 'radios',
    '#title' => t('Select a Region'),
    '#required' => TRUE,
    '#options' => $options,
    '#description' => t('These are regions in the current default theme that contain blocks. You can edit existing blocktable configurations or create new ones.'),
  );
}

/**
 * Builds form fields.
 *
 * @param $form
 *  The multiform form argument.
 * @param $form_state
 *  The multiform state argument.
 */
function add_step_2_fields(&$form, &$form_state) {
  // If there is an existing Blocktable definition for this region,
  // use it to set default values here.
  $region = $form_state['storage']['region'];
  $blocks = $form_state['storage'][$region]['blocks'];
  $defined = FALSE;
  $definitions = $form_state['storage']['definitions'];
  if ($definitions[$region]) {
    $defined = TRUE;
    $definition = $definitions[$region];
  }

  // Add list of blocks preceding table markup textarea.
  $blocks = $form_state['storage'][$region]['blocks'];
  $block_info = '<div class="form-item">
<label>' . t('Region: ') . $region .  '</label><p>' . t("Blocks for this region. The Block IDs should be embedded in the table markup, wrapped in colons (':').") . '</p></div>';
  $block_info .= '<table><tr><th>Block ID</th><th>Module</th><th>Title</th></tr>';
  foreach ($blocks as $block) {
    $block_info .= "<tr><td>$block->bid</td><td>$block->module</td><td>$block->title</td></tr>";
  }
  $block_info .= '</table>';
  $form['step_message']['block_info'] = array(
    '#value' => $block_info,
  );
  $form['step_message']['markup'] = array(
    '#type' => 'textarea',
    '#title' => t('Markup'),
    '#description' => t("Enter table markup with block indicators. Line breaks don't matter."),
    '#required' => TRUE,
    '#rows' => 5,
    '#cols' => 40,
    '#default_value' => $definition['markup'],
  );

  $visibility_options = array(0 => t('Show on every page except the listed pages.'),
                              1 => t('Show on only the listed pages.'), );
  if (user_access('use PHP for blocktable visibility')) {
    $visibility_options[2] = t('Show if the following PHP code returns TRUE (PHP-mode, experts only).');
    $php_description = "If the PHP-mode is chosen, enter PHP code between &lt;?php ?&gt;. Note that executing incorrect PHP-code can break your Drupal site.";
  }
  else {
    $php_description = '';
  }
  $form['step_message']['visibility'] = array(
    '#type' => 'radios',
    '#title' => t('Show this Blocktable on Specific Pages'),
    '#options' => $visibility_options,
    '#default_value' => ($definition['visibility']) ?  $definition['visibility'] : 1,
  );

  $form['step_message']['pages'] = array(
    '#type' => 'textarea',
    '#title' => t('Pages'),
    '#description' => t("Enter one page per line as Drupal paths. The '*' character is a wildcard. Example paths are <em>blog</em> for the blog page and <em>blog/*</em> for every personal blog. <em>&lt;front&gt;</em> is the front page. $php_description"),
    '#rows' => 2,
    '#cols' => 40,
    '#default_value' => $definition['pages'],
  );
}
