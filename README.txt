
CONTENTS OF THIS FILE
---------------------
 * Introduction
 * Terminology
 * Requirements
 * Installation
 * Configuration
 * Table Markup
 * Styling
 * Disabling  (s)
 * Uninstalling Blocktable
 * Limitations
 * Extra Feature
 * Contact

INTRODUCTION
------------

Blocktable embeds all the blocks in a region into an HTML table. 

This can useful for achieving certain layout requirements, such as arranging
blocks in a multi-column format.

TERMINOLOGY
-----------

'Blocktable', with an uppercase 'B', refers to the Blocktable module, 
and 'blocktable', with a lowercase 'b' refers to an individual table 
defined for a particular region. 


REQUIREMENTS
------------

None.


INSTALLATION
------------

Install as usual, see http://drupal.org/node/70151 for further information.


CONFIGURATION
-------------

0. Check the region that you want to use a blocktable in. On the Blocks page, 
   Administer -> Site Building -> Blocks, make sure the correct blocks are in 
   the region, and in the same order that they will appear in in the table 
   markup.
   
1. Configure user permissions in Administer >> User management >> Permissions >>
   blocktable module:

   - administer blocktables
    
     Users in roles with the "administer blocktables" permission can create or 
     edit blocktables.
      
   - use PHP for blocktable visibility
  
     Users in roles with the "use PHP for blocktable visibility" permission 
     can provide php to be used to determine which pages a blocktable is
     applied to. This closely follows the logic in block.module. 

2. Edit the block template for the region which is getting the blocktable. 
   For example, if you want to put a blocktable in the content region, edit 
   block-content.tpl.php. If this file does not exist in your theme, copy 
   block.tpl.html to block-content.tpl.html. It is possible to use 
   block.tpl.php, but it will hurt performance somewhat.

   Add a call to Blocktable near the top of the block template file. This 
   must be placed right after the <?php $Id ... > section and before any HTML 
   is generated. Usually a <div> is the first HTML. Here is the line to add:  
     <?php if (function_exists(blocktable_make_table)) {print blocktable_make_table($block, 'start');} ?>
   Add another call to Blocktable at the bottom, after all HTML has been 
   generated. Usually a </div> or comment is the last HTML. Here is the
   line to add:
     <?php if (function_exists(blocktable_make_table)) {print blocktable_make_table($block, 'end');} ?>

3. To configure a blocktable, go to Administer -> Site Building -> Blocktable.
   Configuration is a two step process. 
   1. Choose a region from the list. Only regions that are in the current 
      default theme and contain blocks are shown. Each region has a brief 
      description: 
      - 'Create new blocktable' means there is no blocktable defined for 
        this region. 
      - 'Edit existing blocktable' means there is a definition that you can 
        edit. 
      - 'Edit existing blocktable - Currently invalid' means that the existing 
        blocktable definition is invalid because the blocks in the definition 
        no longer match the blocks in the region. This is checked everytime a 
        page is built, and Blocktable will not add table markup to a region 
        if the definition is invalid. You must edit the blocktable definition 
        so that the markup matches the blocks in the region. 
   2. Enter the table markup you want to use for the region. See below for 
      details on table markup. You can also specify the pages where the 
      blocktable should appear. This uses the same system as the blocks 
      themselves. You probably want to set this up similarly to the blocks 
      which are in the region. 
      

TABLE MARKUP
------------

You must supply your own table markup. Blocktable table markup consists 
of HTML table tags, like <table>, and block IDs wrapped in colons, 
like :37:. The block IDs are placed between, not within, tags, at the place 
where the block should go in the completed page.

Example: Here is a table with 2 rows and 3 columns. 6 blocks (with block 
IDs 35, 33, 37, 23, 40, and 38) are assigned to the cells. Some cells get 
1 block, some get more than 1, some get none. Each block must be assigned 
to exactly one cell.

  <table id="my_id" class="my_class">
    <tr>
      <td>:35::33:</td>
      <td>:37:</td>
      <td>:23:</td>
    </tr>
    <tr>
      <td>:40:</td>
      <td></td>
      <td>:38:</td>
    </tr>
  </table>

There are no restrictions on the table markup. Colspan, rowspan, classes,
styling, etc., are permitted. Markup is passed through to the page with no 
change.

Whitespace (spaces, newlines, etc.) is OK, and is usually passed through 
to the processed page.

Blocks must appear in the table markup in the weighted order they are 
listed in. If you want a different order, go back to the Blocks page and 
change the order there.

If you change the order of the blocks, or add blocks to the region, or 
remove blocks from the region, you must update the blocktable for that 
region. Otherwise Blocktable will not add any table markup. No warning 
is issued when this happens.
  
It may happen that a block that is included in the blocktable definition is 
not included on some page, due to block rules, roles & permissions, etc. 
This is not considered an error, and the table is built without that 
block's contents. However, if the missing block is the last one expected, 
the closing table markup will not be included, which will probably break the 
page. If this problem occurs, you can fix it with a dummy block. Here is one 
way to create a dummy block:
  - On the Blocks page, create a block that consists of an HTML comment, such as
    <!-- Dummy block for Blocktable --> 
  - Assign the block to the region with the blocktable.
  - Make sure it always visible whenever the blocktable is visible.  
  - Make sure it is last in the list (has the heaviest weight).

You can define blocktables for any region that contains blocks.

STYLING
-------

Although there are no restrictions on the table markup, note that the blocks 
in the table cells are wrapped within several layers of divs which have their 
own styling. This can make styling at the table cell level complicated.

No change is made to the blocks besides wrapping them with the table 
markup. However, styling for the blocks, especially anything involving floats 
and positioning, may be affected since the blocks are now enclosed in table
cells.

DISABLING A BLOCKTABLE
----------------------
There is more than one way to temporarily disable a blocktable. Any of 
these will do:
1. Remove both calls to blocktable_make_table in block-<region>.tpl.html.
   This prevents all Blocktable activity during page generation.   
2. Set the path visibility setting so that the blocktable is never visible.
3. Leave the table markup blank in the blocktable configuration.
4. Delete the blocktable_definition entry in the variable table. This will 
   disable ALL blocktables, and if you want to re-implement the blocktables, 
   you will have to repeat the configuration process.

UNINSTALLING BLOCKTABLE
-----------------------
Recommended steps: 
1. Remove both calls (top and bottom) to blocktable_make_table in block-<region>.tpl.html.
2. Disable the Blocktable module. 
3. Uninstall the Blocktable module. This deletes the blocktable_definition 
   entry in the variable table.

LIMITATIONS
-----------

Blocktable only works on blocks. Depending on the theme, some regions, 
like the header, may have components such as logos, slogans, etc., 
that are not blocks. Blocktable won't work properly in those cases. 
You could address this problem by modifying the theme to remove the 
non-block components, and then add them back in within blocks. However, 
this is too theme-dependent and complex to make any recommendations here. 

If you add, remove, or reorder blocks in the region using the Blocks page,
you must make corresponding changes to the blocktable definition. 

EXTRA FEATURE
-------------

The table markup described above can actually be any kind of markup, not 
necessarily a table. You could, for example use this module to embed your 
blocks in an HTML list or any arbitrary HTML. I don't have a particular 
use case for this in mind, but the capability is there.

CONTACT
-------

Current maintainers:
  Joe Casey (Joe Casey) - http://drupal.org/user/823758

